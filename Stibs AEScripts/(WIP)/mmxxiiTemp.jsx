// license below
// more: https://blob.pureandapplied.com.au

(function(){
    this.name = "mmxxiiTemp";
    function doSomethingWithSelectedLayersOrAllIfNoneSelected(doTheThing) {
        var theComp = app.project.activeItem;
        if (theComp) {
            var firstLayer = 0;

            var theLayers = theComp.selectedLayers;
            if (theLayers.length === 0) {
                firstLayer = 1;
                theLayers = theComp.layers;
            }
            for (var lyr = firstLayer; lyr < theLayers.length + firstLayer; lyr++) {
                doTheThing(theLayers[lyr])
            }
        }
    }

    function theThing(theLayer){
        var theProps = theLayer.selectedProperties;
        var theComp = theLayer.containingComp;
        var f = theComp.frameDuration;
        for (var p = 0; p < theProps.length; p++){
            var prop = theProps[p];
            var theKeys = prop.selectedKeys;
            var keyVals = [];
            for (var k = 0; k < theKeys.length; k++) {
                var t = prop.keyTime(theKeys[k] + 1) - f;
                var v = prop.valueAtTime(prop.keyTime(theKeys[k]) + 8 * f, false);
                keyVals[k] = { time: t, value: v }; 
            }
            for (var k = 0; k < theKeys.length; k++) {
                prop.setValueAtTime(keyVals[k].time, keyVals[k].value)
            }
        }
    }

    app.beginUndoGroup(this.name);
        doSomethingWithSelectedLayersOrAllIfNoneSelected(theThing)
    app.endUndoGroup();
})()


// ========= LICENSE ============
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see https://www.gnu.org/licenses/
