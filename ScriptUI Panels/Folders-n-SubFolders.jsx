(function () {
    {
        function FolderSetup(thisObj) {
            var folderSetupData = new Object();
            folderSetupData.scriptName = "Folder Setup";
            folderSetupData.version = "1.2";

            folderSetupData.strAboutTitle = "About " + folderSetupData.scriptName;
            folderSetupData.strAbout = folderSetupData.scriptName + " " + folderSetupData.version + "\n" +
                "This script allows you to describe a hierarchy of folders to create in the Project panel by entering folder names on successive lines, with the indentation implying the nesting level.\n" +
                "\n" +
                "To enter new lines, press Ctrl+Enter (Windows) or Ctrl+Return (Mac) in CS5.5 and earlier, or just Enter/Return in CS6 and later. Use indentation at the beginning of a line to indicate that you want a folder to be created inside the previous folder.\n" +
                "\n" +
                "You can use this script as a dockable panel by placing it in a ScriptUI Panels subfolder of the Scripts folder, and then choosing the Folder Setup.jsx script from the Window menu.\n" +
                "This version modified from an original of unknown source by stib. More at blob.pureandapplied.com.au";
            folderSetupData.strFolderHierarchy = "Folder Hierarchy:";
            folderSetupData.strDefaultHierarchy = "Folder 1\n    Subfolder A\n    Sub-subfolder I\n    Sub-subfolder II\n    Subfolder B\nFolder 2\n    Subfolder C\n    Subfolder D\nFolder 3";
            folderSetupData.strCreateFolders = "Create Folders";
            folderSetupData.strHelp = "?";
            folderSetupData.strErrMinAE90 = "This script requires Adobe After Effects CS4 or later.";

            // folderSetup_buildUI()
            // Function for creating the user interface
            function folderSetup_buildUI(thisObj) {
                if (app.settings.haveSetting("folderSetupScript", "defaultHierarchy")) {
                    folderSetupData.strDefaultHierarchy = app.settings.getSetting("folderSetupScript", "defaultHierarchy")
                }
                // pal
                // ======
                var pal = (thisObj instanceof Panel) ? thisObj : new Window("palette", folderSetupData.scriptName, undefined, { resizeable: true });
                pal.text = folderSetupData.scriptName;
                pal.preferredSize.width = 300;
                pal.orientation = "column";
                pal.alignChildren = ["fill", "top"];
                pal.spacing = 10;
                pal.margins = 16;

                // HIERARCHYPANEL
                // ==============
                var hierarchyPanel = pal.add("panel", undefined, undefined, { name: "hierarchyPanel" });
                hierarchyPanel.text = folderSetupData.strFolderHierarchy;
                hierarchyPanel.orientation = "row";
                hierarchyPanel.alignChildren = ["fill", "fill"];
                hierarchyPanel.spacing = 1;
                hierarchyPanel.margins = 10;
                hierarchyPanel.alignment = ["fill", "top"];

                var hierarchy = hierarchyPanel.add(
                    "edittext",
                    undefined,
                    folderSetupData.strDefaultHierarchy,
                    {
                        alignment: ['fill', 'fill'],
                        multiline: true,
                        resizeable: true
                    }
                );
                hierarchy.text = folderSetupData.strDefaultHierarchy;
                hierarchy.multiline = true;

                // btnGrp
                // ======
                var btnGrp = pal.add("group", undefined, { name: "btnGrp" });
                btnGrp.orientation = "row";
                btnGrp.alignChildren = ["right", "center"];
                btnGrp.spacing = 10;
                btnGrp.margins = 0;

                var helpBtn = btnGrp.add("button", undefined, undefined, { name: "helpBtn" });
                helpBtn.text = "?";

                var createBtn = btnGrp.add("button", undefined, undefined, { name: "createBtn" });
                createBtn.text = "Create Folders";

                helpBtn.onClick = function () { alert(folderSetupData.strAbout, folderSetupData.strAboutTitle); }
                hierarchy.text = folderSetupData.strDefaultHierarchy;
                function createFolders() {
                    folderSetup_doCreateFolders(hierarchy.text);
                }
                createBtn.onClick = createFolders;

                pal.layout.layout(true);
                pal.layout.resize();
                pal.onResizing = pal.onResize = function () { this.layout.resize(); }

                pal.onClose = function () {
                    app.settings.saveSetting("folderSetupScript", "defaultHierarchy", hierarchy.text);  
                    app.preferences.saveToDisk();
                }
                return pal;
            }

            // folderSetup_doCreateFolders()
            // Callback function for creating the actual folder structure
            function folderSetup_doCreateFolders(theText) {
                app.settings.saveSetting("folderSetupScript", "defaultHierarchy", theText);
                var folderNames = theText.split("\n"); // Split the lines of edittext content into an array

                if (folderNames.length > 0) {
                    app.beginUndoGroup(folderSetupData.scriptName);

                    var matches;
                    var currParent = lastFolder = app.project.rootFolder, newFolder;
                    var currIndent = 0, newIndent;
                    var blankLine;

                    // Traverse the folder names
                    for (var f = 0; f < folderNames.length; f++) {
                        // Use a regular expression to pick off the indentation and folder name
                        blankLine = folderNames[f].match(/^(\s*)$/);
                        matches = folderNames[f].match(/^(\s*)(.*)$/);
                        if ((matches != null) && ((matches.length == 3) && (!blankLine))) // Skip blank lines
                        {
                            //$.writeln("creating folder named '"+matches[2]+"'");
                            newIndent = parseInt(matches[1].length);

                            // Determine where to place the new folder
                            if (newIndent > currIndent) // Nest new folder
                            {
                                currParent = lastFolder;
                                currIndent++;
                            }
                            else if (newIndent < currIndent) // Create in some parent folder
                            {
                                // Find indent matching newIndent by traversing parent folders
                                while ((currIndent > 0) && (currIndent > newIndent)) {
                                    currParent = currParent.parentFolder;
                                    currIndent--;
                                }
                            }
                            // else create at same level as previous folder (sibling)

                            // Create the new folder in the current parent
                            lastFolder = newFolder = app.project.items.addFolder(matches[2]);
                            newFolder.parentFolder = currParent;
                        }
                    }

                    app.endUndoGroup();
                }
            }

            // main:
            //

            if (parseFloat(app.version) < 9) {
                alert(folderSetupData.strErrMinAE90, folderSetupData.scriptName);
                return;
            }
            else {
                var fsPal = folderSetup_buildUI(thisObj);
                if (fsPal != null) {
                    if (fsPal instanceof Window) {
                        fsPal.center();
                        fsPal.show();
                    }
                    else
                        fsPal.layout.layout(true);
                }
            }
        }

        FolderSetup(this);
    }
})()