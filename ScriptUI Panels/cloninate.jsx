﻿// @target aftereffects
// license below
// more: https://blob.pureandapplied.com.au

// the cloninator clones an item in a comp and creates a
// new source for it in the project (c)2016 Stephen Dixon

/* global app, Panel, $, isValid */
(function (thisObj) {



  var scriptName = 'cloninate';



  function makeUniqueCompName(oldSource, prefix, suffix) {
    if (oldSource.name) {
      if (!suffix) { suffix = '' }
      if (!prefix) { prefix = '' }
      // Create a unique name, given a layer
      // find a serialnumber suffix if one exists e.g. mypic.jpg_1 
      // everyone stand back… 
      // the RE matches any string that ends with '_', '-', or a space,
      // followed by a number. eg foo99bar_9 will match
      // (foo99bar)_(9)
      var re = /(.*[^\d^_])[_\- ](\d*)$/;

      var m = oldSource.name.match(re);
      var oldSourceSerial = m[2];
      var oldSourceBaseName = m[1];

      //default serial number
      var newSourceSerial = 1;

      // if no match, then the source doesn't have a serial number. One of these
      // should catch it
      if (typeof (oldSourceSerial) === 'undefined' || oldSourceSerial === '' || isNaN(parseInt(oldSourceSerial, 10))) {
        // since there was no serial we add a separator onto the base name so that it
        // becomes basename_1 etc
        oldSourceBaseName = oldSource.name + '_';
      } else {
        //there was a serial number, so increment it
        newSourceSerial = 1 + parseInt(oldSourceSerial, 10);
      }

      if (!oldSourceBaseName) {
        oldSourceBaseName = oldSource.name;
      } //shouldn't happen, but you know, regex..
      // we need to check to see if a source layer with the new serial number exists,
      // and if it does we keep incrementing the serial until it doesn't
      while (findDuplicateSourceItems('' + oldSourceBaseName + newSourceSerial)) {
        newSourceSerial++;
      }

      return prefix + oldSourceBaseName + suffix + '_' + newSourceSerial;
    } else {
      return false;
    }
  }

  function findDuplicateSourceItems(theName) {
    var allItems = app.project.items;
    var j;
    for (j = 1; j <= allItems.length; j++) {
      if (app.project.items[j].name === theName) {
        return true;
      }
    }
    return false;
  }

  function reinstateButton(theButton) { //reinstate a button with an oldValue property
    if (!theButton.enabled) { //turn on the footage button and reinstate its value if needs be
      theButton.value = theButton.oldValue;
      theButton.enabled = true;
    }
  }

  function getSiblings(theItem) {
    // returns 1-index collection
    if (theItem.containingComp) {
      return theItem.containingComp.layers;
    }
    return app.project.items;
  }

  function setupCloninate(theItems, cloneSettings) {
    app.beginUndoGroup('cloninator');
    for (i = 0; i < theItems.length; i++) {
      // because cloninating adds items to the layer collection
      // or project items collection, it invalidates references
      // to other layers. So we have to tag the layer so we can 
      // find it again.
      theItems[i].needsCloninating = true;
    }
    // siblings is 1-indexed
    var siblings = getSiblings(theItems[0])
    for (i = 1; i <= siblings.length; i++) {
      var ogItem = siblings[i];
      if (ogItem.needsCloninating) {
        cloninate(ogItem, cloneSettings)
      }
    }
    app.endUndoGroup();
  }

  function cloninate(ogItem, cloneSettings) {
    if (cloneSettings.inifiniteRecursion ||
      cloneSettings.recursionDepth <= cloneSettings.recursionLimit) {
      ogItem.needsCloninating = null;
      ogItem.wasCloninated = true;
      if (ogItem instanceof AVLayer) {
        return cloninateAVLayer(ogItem, cloneSettings)
      } else if (ogItem instanceof FootageItem) {
        ogItem.replace(cloninateFootage(ogItem, cloneSettings).file);
      } else if (ogItem instanceof CompItem) {
        return cloninateComp(ogItem, cloneSettings)
      } else if (
        ogItem instanceof CameraLayer ||
        ogItem instanceof LightLayer ||
        ogItem instanceof ShapeLayer ||
        ogItem instanceof TextLayer
      ) {
        return cloninateSourcelessLayer(ogItem, cloneSettings);
      } else {
        ogItem.wasCloninated = false;
        cloneSettings.info("No items to cloninate!")
        return false
      }
    }
  }

  function cloninateFootage(ogItem, cloneSettings) {
    // cloninate a source footage item
    if (cloneSettings.inifiniteRecursion || cloneSettings.recursionDepth <= cloneSettings.recursionLimit) {
      // footageItem in project window
      var newItem = app.project.importFile(new ImportOptions(ogItem.file));
      // synch original and new item
      var props = [
        "alphaMode",
        "removePulldown",
        "premulColor",
        "nativeFrameRate",
        "loop",
        "isStill",
        "invertAlpha",
        "highQualityFieldSeparation",
        "hasAlpha",
        "fieldSeparationType",
        "displayFrameRate",
        "conformFrameRate"
      ]
      for (var k = 0; k < props.length; k++) {
        try {
          newItem.mainSource[props[k]] = ogItem.mainSource[props[k]];
        } catch (e) {
          $.writeln(e);
        }
      }
      return newItem;
    }
  }

  function cloninateAVLayer(ogItem,
    cloneSettings) {
    if (cloneSettings.inifiniteRecursion || cloneSettings.recursionDepth <= cloneSettings.recursionLimit) {
      // item is a layer in a comp with a source
      ogItem.cloninateShouldDelete = cloneSettings.replaceOriginal;
      var newItem = ogItem.duplicate();
      var newItemSource = cloninate(newItem.source, cloneSettings);
      newItem.replaceSource(newItemSource, true);
      var parentComp = newItem.containingComp;
      for (var i = parentComp.numLayers; i > 0; i--) {
        if (parentComp.layer(i).cloninateShouldDelete) {
          parentComp.layer(i).remove();
        }
      }
      return newItem;
    }
  }

  function cloninateComp(ogItem, cloneSettings) {
    // duplicate an comp, making its layers unique
    if (cloneSettings.inifiniteRecursion || cloneSettings.recursionDepth <= cloneSettings.recursionLimit) {
      // item is either comp in project window
      // or in the currently active comp.
      var newComp = ogItem.duplicate();
      newComp.name = makeUniqueCompName(ogItem.name);
      var newCloneSettings = cloneSettings;
      newCloneSettings.replaceOriginal = true;
      newCloneSettings.recursionDepth += 1;
      for (var i = 1; i <= newComp.numLayers; i++) {
        cloninate(
          newComp.layer(i),
          newCloneSettings)
      }
      return newComp;
    }
  }

  function cloninateSourcelessLayer(ogItem, cloneSettings) {
    // only have to do anything if the original item is being duplicated, not replaced
    if (!cloneSettings.replaceOriginal) {
      return ogItem.duplicate();
    }
  }

  function getSelectedItems() {
    var results;
    if (app.project.activeItem.selectedLayers.length) {
      results = app.project.activeItem.selectedLayers;
    } else if (app.project.activeItem) {
      results = [app.project.activeItem];
    } else {
      results = app.project.selection;
    }
    return results;
  }

  function buildUI(thisObj) {
    var cloninateBttn;
    var replacinateBttn;
    var recurseGrp;
    var levelGroup;
    var infiniteRecurseChkBx;
    var recursionLimitTextBx;
    var recurseIntoCompsBtn;
    var btnGrp;
    var infoText;
    var pal = thisObj;
    if (!(pal instanceof Panel)) {
      pal = new Window('palette', scriptName, undefined, { resizeable: true });
    }

    if (pal !== null) {
      btnGrp = pal.add('group', undefined, { orientation: 'row' });
      cloninateBttn = btnGrp.add('button', [
        undefined, undefined, 90, 22
      ], 'cloninate');
      replacinateBttn = btnGrp.add('button', [
        undefined, undefined, 90, 22
      ], 'replacinate');

      recurseIntoCompsBtn = pal.add('checkbox', [
        undefined, undefined, 180, 22
      ], ' recurse into comps');

      recurseGrp = pal.add('panel', undefined, 'recursion level', { alignChildren: "left" });
      recurseGrp.orientation = 'column';

      levelGroup = recurseGrp.add('group', undefined, {
        orientation: 'row',
        alignChildren: "left"
      });
      levelGroup.alignChildren = ['left', 'center'];
      infiniteRecurseChkBx = levelGroup.add('checkbox', [
        undefined, undefined, 100, 22
      ], 'infinite');
      recursionLimitTextBx = levelGroup.add('editText', [
        undefined, undefined, 30, 22
      ], '1');
      infoText = pal.add("statictext", undefined, 180, 22)

      levelGroup.add('staticText', undefined, 'limit:');
      recursionLimitTextBx.enabled = true;
      recursionLimitTextBx.width = 40;
      infiniteRecurseChkBx.value = false;
      recurseIntoCompsBtn.value = false;
      recurseIntoCompsBtn.oldValue = false; // see below

      infiniteRecurseChkBx.onClick = function () {
        if (infiniteRecurseChkBx.value) {
          reinstateButton(recurseIntoCompsBtn);
          recursionLimitTextBx.enabled = false; //infinite overrides the limit
        } else {
          recursionLimitTextBx.enabled = true;
          if (parseInt(recursionLimitTextBx.text, 10) === 0) {
            // both recursion methods are off so turn off the footage switch remember the
            // value so it can be reinstated when I uncheck the "selected only" checkbox
            recurseIntoCompsBtn.oldValue = recurseIntoCompsBtn.value;

            //turn it off so it's unambiguous
            recurseIntoCompsBtn.value = false;
            recurseIntoCompsBtn.enabled = false;
          }
        }
      };

      recursionLimitTextBx.onChange = function () {
        if (isNaN(parseInt(recursionLimitTextBx.text, 10))) {
          recursionLimitTextBx.text = 0;
        }

        if (parseInt(recursionLimitTextBx.text, 10) === 0) { //user set no recursion
          infiniteRecurseChkBx.value = false;

          // recursion is off so turn off the footage switch and remember the value so it can
          // be reinstated when I uncheck the "selected only" checkbox
          recurseIntoCompsBtn.oldValue = recurseIntoCompsBtn.value;

          //turn it off so it's unambiguous
          recurseIntoCompsBtn.value = false;
          recurseIntoCompsBtn.enabled = false;
        } else if (parseInt(recursionLimitTextBx.text, 10) < 0) { //user set <0 recursion => infinite
          infiniteRecurseChkBx.value = true;
          recursionLimitTextBx.enabled = false;
          reinstateButton(recurseIntoCompsBtn);
        } else if (parseInt(recursionLimitTextBx.text, 10) > 0) { //user set limited recursion
          infiniteRecurseChkBx.value = false;
          reinstateButton(recurseIntoCompsBtn);
        }
      };


      cloninateBttn.onClick = function () {
        // do the hoo-hah
        var selectedItems = getSelectedItems();
        var inifiniteRecursion = infiniteRecurseChkBx.value;
        var recursionLimit = parseInt(recursionLimitTextBx.text, 10);
        var cloneSettings = {
          inifiniteRecursion: inifiniteRecursion,
          recursionLimit: recursionLimit,
          replaceOriginal: false,
          recursionDepth: 0,
          info: function (txt) { infoText.text = txt }
        }
        setupCloninate(selectedItems, cloneSettings)
      }

      replacinateBttn.onClick = function () {
        // do the hoo-hah
        var originalItems = app.project.activeItem.selectedLayers;
        var i;
        // var layerHistory = [];
        // recursionLimit of -1 == infinite recursion
        var recursionLimit = (infiniteRecurseChkBx.value)
          ? -1
          : parseInt(recursionLimitTextBx.text, 10);


        app.beginUndoGroup('cloninator');
        var originalIsCompInProjectWindow = (originalItems.length === 0);
        if (!originalIsCompInProjectWindow) {
          for (i = 0; i < originalItems.length; i++) {
            cloninate(originalItems[i], recursionLimit, recurseIntoCompsBtn.value, false, 0);
            originalItems[i].remove();
          }
        }
        app.endUndoGroup();
      };
    }

    if (pal instanceof Window) {
      pal.center();
      pal.show();
    } else {
      pal.layout.layout(true);
    }
  }

  function isTypeOf(anItem, type) {
    var TYPES = {
      composition: /Komposition|Composición|Composition|Composizione|コンポジション|컴포지션|Composição|Композиция|合成/,
      folder: /Folder|Ordner|Carpeta|Dossier|Cartella|フォルダー|폴더|Pasta|Папка|文件夹/,
      footage: /Footage|Material de archivo|Métrage|Metraggio|フッテージ|푸티지|Gravação|Видеоряд|素材/

    }
    if (TYPES[type]) {
      return TYPES[type].test(anItem.typeName)
    }
    return false
  }

  buildUI(thisObj);
})(this)
//cloninate(app.project.activeItem.selectedLayers[0], -1, true, false, 0);

//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see https://www.gnu.org/licenses/



